// Start of Script (loginstatus.js)

function changeInnerHTML(divId,html){
 if (document.getElementById) {
       document.getElementById(divId).innerHTML= html;
 }else{
     document.layers[divId].document.open();
     document.layers[divId].document.write(html);
     document.layers[divId].document.close();
 }
}

function isDefined(obj) {
  return(!(obj === undefined));
}

function sessionExpireDate(timeout) {
   months = Array('Jan.','Feb.','Mar.','Apr.','May','Jun.','Jul.','Aug.','Sep.','Oct.','Nov.','Dec.');
   expireDate = new Date();
   expireDate.setTime(expireDate.getTime()+(timeout*1000));
   y = expireDate.getFullYear();
   m = expireDate.getMonth();
   d = expireDate.getDate();
   h = expireDate.getHours();
   n = expireDate.getMinutes();
   if (n<10) n = '0'+n;
   return ''+months[m]+' '+d+', '+y+' '+h+':'+n;
}

function sessionExpireCountdown(timeout) {
   // Calculate the number of days left
   var days = Math.floor(timeout / 86400);
   // After deducting the days calculate the number of hours left
   var hours = Math.floor((timeout - (days * 86400 ))/3600)
   // After days and hours , how many minutes are left
   var minutes = Math.floor((timeout - (days * 86400 ) - (hours *3600 ))/60)
   // Finally how many seconds left after removing days, hours and minutes.
   var secs = Math.floor((timeout - (days * 86400 ) - (hours *3600 ) - (minutes*60)))

   var retval = "" 
   if (days > 1) retval += days + ' days ';
   else if (days == 1) retval += days + ' day ';

   if (hours > 1) retval += hours + ' hours ';
   else if (hours == 1) retval += hours + ' hour ';
   
   if (minutes > 1) retval += minutes + ' minutes ';
   else if (minutes == 1) retval += minutes + ' minute ';

   if (retval != "") retval += ' and ';
   if (secs > 1) retval += secs + ' seconds ';
   else if (secs == 1) retval += + secs + ' second ';
   else if (secs == 0) retval += + secs + ' second ';

   return retval;
}

function updateStatusMessage(data_quota,originalTimeout,pageLoadTime) {
    	var status_Message = '';
	
	currentTime = new Date();
    	currentSessionTimeout = originalTimeout - ( ((currentTime.getTime())-(pageLoadTime.getTime()))/1000 );

	if ( currentSessionTimeout < 0 )
        	currentSessionTimeout = 0;

	if (data_quota)
	{
		this.status_Message=login_msg_base+data_quota+quota_unit+" quota or "+sessionExpireCountdown(currentSessionTimeout)+' later (i.e. '+sessionExpireDate(currentSessionTimeout)+').';
	}
	else
	{
		this.status_Message=login_msg_base+sessionExpireCountdown(currentSessionTimeout)+' later (i.e. '+sessionExpireDate(currentSessionTimeout)+').';
	}

   	changeInnerHTML('statusbox',this.status_Message);

	if (currentSessionTimeout)
	   	setTimeout('updateStatusMessage(dataQuota,sessionTimeout,pageLoadTime)',1000);
}

var sessionTimeout = 0;
var quota_unit = ' MB';
var login_msg_base = "This session will end when usage reaches ";
var dataQuota = 0;

if (isDefined(status_DataQuota)) {
	// EDDY: Update DataQuota to be MB so need to div 1024 / 1024
	dataQuota = (status_DataQuota / 1024 / 1024);
}
if (isDefined(status_SessionTimeout)) {
	sessionTimeout = status_SessionTimeout;

	if (!sessionTimeout)
	{
		if (!dataQuota)
		{		
			this.status_Message = 'This account has unlimited access.';
		}
		else
		{
			this.status_Message=login_msg_base+dataQuota+quota_unit+' quota.';
		}

		changeInnerHTML('statusbox', this.status_Message);
	}
	else
	{
		var pageLoadTime = new Date();
		updateStatusMessage(dataQuota,sessionTimeout,pageLoadTime);
	}
}

// End of Script (loginstatus.js)
