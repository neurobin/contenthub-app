// Start of Script (menulib.js)

function create_form(name, method, action, onreset)
{
	var obj = $("<form/>");

	if (name) obj.attr("name", name);
	if (method) obj.attr("method", method);
	if (action) obj.attr("action", action);
	if (onreset) obj.attr("onreset", onreset);
	
	return obj;
}

function create_textarea(name, id, rows, cols, class_name)
{
	var obj = $("<textarea/>");

	if (name) obj.attr("name", name);
	if (id != null) obj.attr("id", id);
	if (rows) obj.attr("rows", rows);
	if (cols) obj.attr("cols", cols);
	if (class_name)	obj.addClass(class_name);
	
	return obj;
}

function create_hr(size, class_name)
{
	var obj = $("<hr/>");	
	obj.attr("size", (size == undefined? 0: size));
	if (class_name)	obj.addClass(class_name);

	return obj;
}

function create_thead()
{
	var obj = $("<thead/>");
	
	return obj;
}

function create_br()
{
	var obj = $("<br/>");
	
	return obj;
}

function create_font(color)
{
	var obj = $("<font/>");
	
	if (color) obj.attr("color", color);
	
	return obj;
}

function create_i(content, class_name)
{
	var obj = $("<i/>");

	if (content) obj.html(content);
	if (class_name)	obj.addClass(class_name);
	
	return obj;
}

function create_center()
{
	var obj = $("<center/>");
	
	return obj;
}

function create_label(for_val, content, id)
{
	var obj = $("<label/>");

	if (for_val) obj.attr("for", for_val);
	if (id != null)	obj.attr("id", id);

	if (content) obj.html(content);

	return obj;
}

function create_img(src, class_name, href)
{
	var obj = $("<img/>");
	
	if (src)
	{
		obj.attr("src", src);
	}

	if (class_name)
		obj.addClass(class_name);

	if (href) obj.attr("href", href);

	return obj;
}

function create_hyperlink(content, href, class_name, help_title, data_help)
{
	var obj = $("<a/>").html(content);	

	if (href) obj.attr("href", href);
	if (class_name) obj.addClass(class_name);
	if (help_title) obj.attr("help_title", help_title);
	if (data_help) obj.attr("data-help", data_help);

	return obj;
}

function create_button(html_content, class_name, click_func, id)
{
	var obj;

	if (id != null)
		obj = $("<button/>", {id: id});
	else
		obj = $("<button/>");

	if (html_content)
		obj.html(html_content);

	if (class_name)
	{
		obj.addClass(class_name);
	}

	if (click_func)
	{
		obj.click(function() {
			click_func();
		});
	}

	return obj;
}

function create_input(id, class_name, type, value, name_val)
{
	var obj;
	if (id != null)
		obj = $("<input/>", {id: id});
	else
		obj = $("<input/>");

	if (class_name)
		obj.addClass(class_name);

	if (type)
		obj.attr("type", type);

	if (value != null)
		obj.attr("value", value);

	if (name_val)
		obj.attr("name", name_val);

	return obj;
}

function insert_hidden_input(obj, list)
{
	if (!list || !list.length) return;

	for (var i = 0, len = list.length; i < len; i++)
	{
		var name = list[i];
		obj.append(create_input(name, "", "hidden", "", name));
	}
}

function create_p(id, class_name, html_content)
{
	var obj;
	if (id != null)
		obj = $("<p/>", {id: id});
	else
		obj = $("<p/>");

	if (class_name)
		obj.addClass(class_name);

	if (html_content)
		obj.html(html_content);

	return obj;
}

function create_div(id, class_name, html_content)
{
	var obj;
	if (id != null)
		obj = $("<div/>", {id: id});
	else
		obj = $("<div/>");

	if (class_name)
		obj.addClass(class_name);

	if (html_content)
		obj.html(html_content);

	return obj;
}

function create_span(id, class_name, html_content)
{
	var obj;
	if (id != null)
		obj = $("<span/>", {id: id});
	else
		obj = $("<span/>");

	if (class_name)
		obj.addClass(class_name);

	if (html_content)
		obj.html(html_content);

	return obj;
}

function create_select(id, obj_name, class_name, html_content)
{
	var obj;

	if (id != null)
		obj = $("<select/>", {id: id});
	else
		obj = $("<select/>");

	if (obj_name)
		obj.attr("name", obj_name);

	if (class_name)
		obj.addClass(class_name);

	if (html_content)
		obj.html(html_content);

	return obj;
}

function create_option(value, html_content, class_name)
{
	var obj = $("<option/>");

	if (value != null)
		obj.attr("value", value);

	if (html_content)
		obj.html(html_content);

	if (class_name)
		obj.addClass(class_name);

	return obj;
}

function create_ul(class_name)
{
	var obj = $("<ul/>");

	if (class_name)
		obj.addClass(class_name);

	return obj;
}

function create_li(class_name, html_content)
{
	var obj = $("<li/>");

	if (class_name)
		obj.addClass(class_name);

	if (html_content)
		obj.html(html_content);

	return obj;
}

function create_table(class_name, html_content, id)
{
	var obj;

	if (id != null)
		obj = $("<table/>", {id: id});
	else
		obj = $("<table/>");

	if (class_name)
		obj.addClass(class_name);

	if (html_content)
		obj.html(html_content);

	return obj;
}

function create_tbody(id, class_name, html_content)
{
	var obj;

	if (id != null)
		obj = $("<tbody/>", {id: id});
	else
		obj = $("<tbody/>");

	if (class_name)
		obj.addClass(class_name);

	if (html_content)
		obj.html(html_content);

	return obj;
}

function create_table_row(class_name, id)
{
	var obj;

	if (id != null)
		obj = $("<tr/>", {id: id});
	else
		obj = $("<tr/>");

	if (class_name)
		obj.addClass(class_name);

	return obj;
}

function create_table_cell(cell_content, class_name, id)
{
	var obj;

	if (id != null)
		obj = $("<td/>", {id: id});
	else
		obj = $("<td/>");

	if (cell_content)
		obj.html(cell_content);
	
	if (class_name)
		obj.addClass(class_name);

	return obj;
}

function create_table_title(content, colspan)
{
	return create_table_row("tabletitle", "").append(
		create_table_cell(content).attr("colspan", colspan)
	);
}

// End of Script (menulib.js)

