"""aces_onboard URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.views.generic import RedirectView

import aces_onboard.urls

import views

import importlib
import os

DJANGO_SETTINGS_MODULE=os.environ['DJANGO_SETTINGS_MODULE']
settings = importlib.import_module(DJANGO_SETTINGS_MODULE)

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='/static/index.html'), name='home'),
    url(r'^favicon.ico$', RedirectView.as_view(url='/static/favicon.ico')),
    url(r'^captive$', views.captive),
    url(r'^(?P<url>(?:v[1-2]/)?register/)$', views.RegisterView.as_view(base_url=settings.ACES_BASE_URI)),
    url(r'^(?P<url>(?:v[1-2]/)?catalog/.*)$', views.CatalogView.as_view(base_url=settings.ACES_BASE_URI)),
    url(r'^(?P<url>(?:v[1-2]/)?decryption/.*)$', views.DecryptionView.as_view(base_url=settings.ACES_BASE_URI)),
    url(r'^(?P<url>(?:v[1-2]/)?statistics/.*)$', views.StatisticsView.as_view(base_url=settings.ACES_BASE_URI)),
    url(r'^(?P<url>(?:v[1-2]/)?satisfaction/.*)$', views.SatisfactionView.as_view(base_url=settings.ACES_BASE_URI)),
    url(r'^(?P<url>(?:v[1-2]/)?survey/.*)$', views.SurveyView.as_view(base_url=settings.ACES_BASE_URI)),
] + aces_onboard.urls.urlpatterns
