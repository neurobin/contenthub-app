#!/bin/sh -x

echo 'Now starting development server...'

cd aces_onboard
PYTHONPATH="$ADAPTIVE_PYTHONPATH" python manage.py collectstatic --no-input
PYTHONPATH="$ADAPTIVE_PYTHONPATH" python manage.py runserver --nostatic "0.0.0.0:$SERVER_PORT"
cd ..
