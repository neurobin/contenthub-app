# git reset --hard
rm -fv env/bin/python
find . -type f -and \( -name '*.pyc' -or -name '*.old' \) -print0 | xargs -0 rm -fv
rm -Rfv env
rm -fv aces_onboard/db.sqlite3
rm -fv contenthub-app.tar.gz

rm -Rfv static/*
rm -fv data/satisfaction.json
rm -Rfv data/sync/*

rm -fv *.log
