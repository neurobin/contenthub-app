#!/bin/sh



. ./script/env.sh



if [ ! -e $ADAPTIVE_VIRTUAL_ENV ]
then
  virtualenv --no-site-packages env
  . env/bin/activate
  pip install -r requirements.txt
  deactivate
fi

cp -fv "$PYTHON" "$ADAPTIVE_VIRTUAL_ENV/bin/"

cp -fv $ADAPTIVE_VIRTUAL_ENV/bin/activate $ADAPTIVE_VIRTUAL_ENV/bin/activate.old
cp -fv $ADAPTIVE_VIRTUAL_ENV/bin/pip $ADAPTIVE_VIRTUAL_ENV/bin/pip.old
cp -fv $ADAPTIVE_VIRTUAL_ENV/bin/gunicorn $ADAPTIVE_VIRTUAL_ENV/bin/gunicorn.old
cp -fv $ADAPTIVE_VIRTUAL_ENV/bin/ipython $ADAPTIVE_VIRTUAL_ENV/bin/ipython.old

sed -i 's/^VIRTUAL_ENV=.*$/VIRTUAL_ENV="$ADAPTIVE_VIRTUAL_ENV"/' "$ADAPTIVE_VIRTUAL_ENV/bin/activate"
sed -i "s|^\#\!.*$|\#\!$ADAPTIVE_VIRTUAL_ENV/bin/python|" "$ADAPTIVE_VIRTUAL_ENV/bin/pip"
sed -i "s|^\#\!.*$|\#\!$ADAPTIVE_VIRTUAL_ENV/bin/python|" "$ADAPTIVE_VIRTUAL_ENV/bin/gunicorn"
sed -i "s|^\#\!.*$|\#\!$ADAPTIVE_VIRTUAL_ENV/bin/python|" "$ADAPTIVE_VIRTUAL_ENV/bin/ipython"

. ./script/test.sh

rm -fv env/bin/python
find . -type f -and \( -name '*.pyc' -or -name '*.old' \) -print0 | xargs -0 rm -fv

tar czf ../contenthub-app.tar.gz .aws_config aces_onboard CHANGES conf config.py data env requirements.txt script start.sh  stop.sh VERSION lib/whitenoise/setup.cfg lib/whitenoise/setup.py lib/whitenoise/tox.ini lib/whitenoise/whitenoise

cp CHANGES VERSION ..
