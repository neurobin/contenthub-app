#!/bin/sh -x

echo 'Now starting production server...'

cd aces_onboard
PYTHONPATH="$ADAPTIVE_PYTHONPATH" python manage.py collectstatic --no-input
PYTHONPATH="$ADAPTIVE_PYTHONPATH" gunicorn -c gunicorn_settings.py aces_onboard.wsgi:application
cd ..
