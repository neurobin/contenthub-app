import logging
import os
import re
import json
import sys
import urlparse
import zipfile
try: # Python 3
    from urllib.request import urlopen
except ImportError:  # Python 2
    from urllib2 import urlopen
import config
import log
import ftputil


DEBUG = True
LOG_DEBUG = DEBUG

sync_output_path = './data/sync/'

if not os.path.exists(sync_output_path):
    os.makedirs(sync_output_path)

aces_host = config.ACES_HOST
aces_s3_media_bucket = config.ACES_S3_MEDIA_BUCKET
aces_s3_secure_bucket = config.ACES_S3_SECURE_BUCKET
aces_s3_media_bucket_url = config.ACES_S3_MEDIA_URL
aces_s3_secure_bucket_url = config.ACES_S3_SECURE_URL
host = config.LOCAL_BASE_URI

s3_url_regex = re.compile(r'^%s' % (config.ACES_S3_URL,))
s3_cover_url_regex = re.compile(r'^%s/covers/' % (aces_s3_media_bucket_url,))
s3_pdf_url_regex = re.compile(r'^%s/.*\.pdf$' % (aces_s3_secure_bucket_url,))
s3_html_url_regex = re.compile(r'^%s/html/' % (aces_s3_media_bucket_url,))
s3_image_url_regex = re.compile(r'^%s/images/' % (aces_s3_media_bucket_url,))
s3_video_url_regex = re.compile(r'^%s/videos/' % (aces_s3_media_bucket_url,))
s3_archive_url_regex = re.compile(r'^%s/arche?ives?/' % (aces_s3_media_bucket_url,))

down_list = []

class MyFTP:
    def __init__(self):
        self.ftp = ftputil.FTPHost(config.FTP_HOST, config.FTP_USER, config.FTP_PASS)
    
    def is_file(self, filename):
        current = self.ftp.pwd()
        try:
            self.ftp.cwd(filename)
        except:
            self.ftp.cwd(current)
            return True
        self.ftp.cwd(current)
        return False

    def download_file(self, rmt, loc):
        self.ftp.download(rmt, loc)

    def download_tree(self, folder, dest, logger):
        for item in self.ftp.walk(folder):
            locdir = os.path.join(dest, item[0])
            logger.debug("Creating dir " + locdir)
            if not os.path.exists(locdir):
                os.makedirs(locdir)
            for subdir in item[1]:
                logger.debug("Subdirs " +  subdir)
            for file in item[2]:
                logger.debug(r"Downloading file {0}/{1}".format(item[0], file))
                self.ftp.download(self.ftp.path.join(item[0],file), os.path.join(locdir,file))

ftp = MyFTP()

class SomethingWrongException(Exception):
    pass


class FTPClient(object):
    """ Client for FTP Sync
    """

    def __init__(self):pass
    
    def download_file(self, rmt, loc):
        try:
            ftp.download_file(rmt, loc)
        except:
            sys.stderr.write('\n\n\nCould not download %s\n\n' % (rmt,))
            return None
        return loc


class AcesClient(FTPClient):
    """ Client for ACES Sync
    """

    def __init__(self, logger):
        super(self.__class__, self).__init__()
        self.logger = logger

    def download_file(self, url):
        url_path = urlparse.urlparse(url).path
        rmt = re.sub('^/'+config.ACES_S3_MEDIA_BUCKET + '/', '', url_path)
        loc = os.path.join(sync_output_path, rmt)
        dirpath = os.path.dirname(loc)
        if not os.path.exists(dirpath):
            os.makedirs(dirpath)
        self.logger.debug('Downloading ' + rmt + ' ...')
        ret = super(self.__class__, self).download_file(rmt, loc)
        if ret is not None:
            self.logger.debug('File stored to %s.' % (loc,))
        return ret

    def synchronize_media(self, url):
        self.logger.debug('Fetch url: %s...' % (url,))
        if s3_cover_url_regex.match(url):
            self.download_file(url)
        elif s3_pdf_url_regex.match(url):
            self.download_file(url)
        elif s3_html_url_regex.match(url):
            self.download_folder(url)
        elif s3_image_url_regex.match(url):
            self.download_file(url)
        elif s3_video_url_regex.match(url):
            self.download_file(url)
        elif s3_archive_url_regex.match(url):
            unzip_archive(self.download_file(url))
        else:
            sys.stderr.write('\nurl = %s\n' % (url,))
            raise SomethingWrongException(
                '!!! Something is wrong in synchronize_media() !!!')

    def download_folder(self, url):
        url_parsed = urlparse.urlparse(url)
        url_path = url_parsed.path
        rpath = re.sub('^/'+config.ACES_S3_MEDIA_BUCKET + '/', '', url_path)
        rdir = os.path.dirname(rpath)
        rdir = '/'.join(rdir.split('/')[0:2])
        ldir = os.path.join(sync_output_path, rdir)
        ldir = os.path.dirname(ldir)
        if rdir not in down_list:
            try:
                self.logger.debug('Downloading ' + rdir + ' ...')
                ftp.download_tree(rdir, sync_output_path, self.logger)
                down_list.append(rdir)
                self.logger.debug('HTML5 Folder stored to %s.' % (ldir,))
            except:
                sys.stderr.write('\nFailed to download ' + rdir + '\n')
                return None
        else:
            self.logger.debug('Skipped multiple download for ' + rdir)
        return ldir



def walk_item(node, urls=[]):
    #~ if DEBUG:
        #~ sys.stderr.write('\twalk_item(%s, %s)\n' %
                         #~ (json.dumps(node), repr(urls)))
    if isinstance(node, dict):
        for key in node.keys():
            walk_item(node[key], urls)
    elif isinstance(node, list):
        for child in node:
            walk_item(child, urls)
    elif isinstance(node, str) or isinstance(node, unicode):
        if s3_url_regex.match(node):
            print(node)
            urls.append(node)
    elif isinstance(node, int):
        pass
    else:
        sys.stderr.write('type(node) = %s\n' % (type(node),))
        raise SomethingWrongException(
            '!!! Something is wrong in walk_item() !!!')


def unzip_archive(filepath):
    if filepath is not None:
        outdir = os.path.splitext(filepath)[0]
        _zipfile = zipfile.ZipFile(filepath)
        _zipfile.extractall(outdir)
    else:
        sys.stderr.write('\nInvalid zip file\n')


def sync_id():
    return 1


def main():

    synchr_id = sync_id()
    level = logging.DEBUG if LOG_DEBUG else logging.INFO
    logger = log.SyncLogger(synchr_id, level=level)
    auth_token = None
    catalog = None

    logger.info(log.LOG_SYNC_INFO_SYNC_START.format(id=synchr_id))
    logger.debug('Connect to Cloud...')
    aces_client = AcesClient(logger)

 
    logger.debug('Fetch catalog...')
    try:
        response = urlopen(config.FTP_CATALOG_URL)
        logger.debug('%s: %s' % (response.getcode(), response.info()))
        catalog = json.loads(response.read().decode(response.info().getparam('charset') or 'utf-8'))
        logger.debug('Success')
        logger.info(log.LOG_SYNC_INFO_GET_CATALOG.format(st=log.LOG_STATUS_OK))
    except Exception as e:
        st = log.LOG_STATUS_ERR_CODE.format(code=response.getcode())
        logger.error(log.LOG_SYNC_INFO_GET_CATALOG.format(st=st))
        logging.exception(e)
        logging.error(response.headers)
        sys.stderr.write('Failed to fetch catalog !\n')
        sys.exit(1)

    with open(sync_output_path + 'catalog.json', 'w') as f:
        f.write(json.dumps(catalog))

    logger.debug('Synchronize medias...')
    logger.info(log.LOG_SYNC_INFO_DL_START.format(st=log.LOG_STATUS_OK))
    logger.catalog(catalog)
    try:
        for available_content in catalog['catalog']:
            for item in available_content:
                try:
                    logger.debug('Synchronize item %s %s %s %s...' %
                                 (item['language'], item['category'][0],
                                  item['category'][1], item['uuid']))
                    logger.content_start(item)
                except: pass
                try:
                    urls = []
                    walk_item(item, urls)
                    urls = list(set(urls)) #uniqify
                    logger.debug('Found %d urls to fetch...' % (len(urls),))
                    for url in urls:
                        aces_client.synchronize_media(url)
                    logger.content_end(item)
                except Exception as e:
                    logger.content_end(item, 0)
                    logging.exception(e)
                    if DEBUG:
                        import ipdb
                        ipdb.set_trace()
        logger.debug(
            'Rewrite the catalog replacing remote urls by local ones...')
        catalog_str = json.dumps(catalog)
        catalog_str = catalog_str.replace(
            aces_s3_media_bucket_url, host + '/static')
        catalog_str = catalog_str.replace(
            aces_s3_secure_bucket_url, host + '/static')
        with open(sync_output_path + 'catalog.json', 'w') as f:
            f.write(catalog_str)

        logger.debug('Done.')
        logger.summary(catalog)

    except Exception as e:
        logging.exception(e)
        sys.stderr.write('Failed to synchronize medias !\n')
        sys.exit(1)


if __name__ == "__main__":
    main()
